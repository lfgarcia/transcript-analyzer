import { CommonModule, PercentPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MinuteSecondPipe } from './pipes/minute-second.pipe';
import { NameSplitPipe } from './pipes/name-split.pipe';
import { SimilarityMatchTooltipPipe } from './pipes/similarity-match-tooltip.pipe';

const materialModules = [
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatSliderModule,
    MatTooltipModule
];

@NgModule({
    declarations: [
        NameSplitPipe,
        MinuteSecondPipe,
        SimilarityMatchTooltipPipe
    ],
    imports: [
        CommonModule,
        FlexModule,
        ...materialModules,
        FormsModule
    ],
    exports: [
        CommonModule,
        FlexModule,
        ...materialModules,
        FormsModule,
        NameSplitPipe,
        MinuteSecondPipe,
        SimilarityMatchTooltipPipe
    ],
    providers: [
        PercentPipe
    ]
})
export class CoreModule { }
