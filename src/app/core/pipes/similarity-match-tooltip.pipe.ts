import { PercentPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import Script from '../models/script.model';

@Pipe({
  name: 'similarityMatchTooltip'
})
export class SimilarityMatchTooltipPipe implements PipeTransform {

  constructor(private percentPipe: PercentPipe) { }

  transform(value: Script): string {
    const tooltip: string[] = [];
    value.matching.forEach(matching => {
      tooltip.push(`${this.percentPipe.transform(value.similarity)} matching with line #${matching.matchingLine + 1} "${matching.matchingSentence}"`);
    });

    let joinStr = '';
    if (tooltip.length > 1) {
      joinStr = '\n\n';
    }
    return tooltip.join(joinStr);
  }

}
