import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'minuteSecond' })
export class MinuteSecondPipe implements PipeTransform {

  transform(value: number | null): string {
    if (value) {
      let minutes = 0;
      while (value - 60 > 0) {
        minutes++;
        value -= 60;
      }
      return `${this.getNumberLessThanTen(minutes)}:${this.getNumberLessThanTen(value)}`;
    }
    return '';
  }

  private getNumberLessThanTen(num: number): string {
    if (num >= 10) {
      return `${num}`;
    }
    return `0${num}`;
  }

}
