import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'nameSplit' })
export class NameSplitPipe implements PipeTransform {

  transform(value?: string | number | null): string {
    if (!value) {
      return 'Unknown';
    }
    const valueStr = `${value}`;
    return valueStr.split(' ')[0];
  }

}
