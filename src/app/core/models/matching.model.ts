import { JsonProperty } from 'json-object-mapper';

export default class Matching {
  @JsonProperty()
  public matchingLine: number;
  @JsonProperty()
  public matchingSentence: string | null;

  constructor() {
    this.matchingLine = -1;
    this.matchingSentence = null;
  }
}
