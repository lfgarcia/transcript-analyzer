import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { CoreModule } from 'src/app/core/core.module';
import AnalyzerCardComponent from './analyzer-card/analyzer-card.component';

import AnalyzerComponent from './analyzer.component';
import { ROUTES } from './analyzer.routes';

@NgModule({
  declarations: [
    AnalyzerComponent,
    AnalyzerCardComponent
  ],
  imports: [
    CoreModule,
    RouterModule.forChild(ROUTES),
    ReactiveFormsModule
  ]
})
export class AnalyzerModule { }
