import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import AgentFacade from 'src/app/core/facades/agent.facade';
import CallFacade from 'src/app/core/facades/call.facade';
import Agent from 'src/app/core/models/agent';
import Call from 'src/app/core/models/call.model';
import Script from 'src/app/core/models/script.model';
import Transcript from 'src/app/core/models/transcript.model';
import TemplateService from 'src/app/core/services/template.service';

@Component({
  selector: 'app-analyzer',
  templateUrl: './analyzer.component.html',
  styleUrls: ['./analyzer.component.scss'],
})
export default class AnalyzerComponent implements OnInit, AfterViewInit {
  @ViewChild('subHeader')
  private subHeader?: TemplateRef<any>;
  public dataSource: any[] = [];
  public dataSourceRep: any[] = [];
  public selectedAgent?: Agent;
  public selectedCall?: Call;
  public selectedSensitivity = .38;
  public call: Transcript | null = null;
  public realPercent = 0;
  public expectedPercent = 0;

  constructor(
    public agents: AgentFacade,
    public calls: CallFacade,
    // tslint:disable-next-line
    private _tplService: TemplateService
  ) {
  }

  public ngAfterViewInit(): void {
    this._tplService.register('subHeader', this.subHeader);
  }

  public ngOnInit(): void {
    this.dataSource = MOCK_DATA();
    this.dataSourceRep = MOCK_DATA().slice(-25);
    this.calls.matchingPercentage$.subscribe(this.calculatePercents);
    this.calls.activeTranscript$.subscribe(res => {
      if (res) {
        this.matchLines(res.transcript, res.script);
        this.matchLines(res.script, res.transcript);
      }
      this.call = res;
      this.calculatePercents();
    });
  }

  public selectAgent(): void {
    if (this.selectedAgent && this.selectedAgent.id) {
      this.agents.setActiveAgent(this.selectedAgent.id);
      this.calls.selectCall('');
      this.selectedCall = undefined;
    }
  }

  public selectCall(): void {
    if (this.selectedCall && this.selectedCall.id) {
      this.calls.selectCall(this.selectedCall.id);
    }
  }

  private calculatePercents = () => {
    if (this.call) {
      const realFound = this.call.transcript.filter(script => script.matching.length > 0
        && script.similarity != null && script.similarity >= this.selectedSensitivity);
      const expectedFound = this.call.script.filter(script => script.matching.length > 0);

      this.realPercent = realFound.length / this.call.transcript.length;
      this.expectedPercent = expectedFound.length / this.call.script.length;
    }
  }

  private matchLines = (actual: Script[], match: Script[]) => {
    actual.forEach(line => {
      line.matching = [];
      line.matchingSentence = (line as any).matching_sentence;
      match.filter(script => script.sentence === (line as any).matching_sentence)
        .forEach(matching => line.matching.push({
          matchingLine: matching.order,
          matchingSentence: matching.sentence
        }));
    });
  }
}

const MOCK_DATA = () => {
  const DATA: any[] = [];
  const SPEAKERS: string[] = [
    'Harvey',
    'Luke'
  ];

  let currentTime = 30;

  for (let i = 0; i < 100; i++) {
    const min = Math.floor(currentTime / 60);
    const sec = Math.floor(currentTime - min * 60);

    DATA.push({
      time: `${(
        '0' + min
      ).slice(-2)}:${(
        '0' + sec
      ).slice(-2)}`,
      speaker: SPEAKERS[Math.floor(Math.random() *
        (
          SPEAKERS.length
        ))],
      sentence: `This is a sample sentence #${i + 1}`
    });

    currentTime +=
      (
        Math.random() * 10
      ) + 5;
  }

  return DATA;
};
