enum RealEnum {
    cardHeading = 'Real',
    cardDescription = 'Alignment to the expected script',
    col01 = 'Time',
    col02 = 'Speaker',
    col03 = 'Sentence',
}

enum ExpectedEnum {
    cardHeading = 'Expected',
    cardDescription = 'Percent of script covered',
    col01 = 'Line',
    col02 = 'Speaker',
    col03 = 'Sentence',
}

export const AnalyzerCardConstants = {
    realEnum: RealEnum,
    expectedEnum: ExpectedEnum
};
