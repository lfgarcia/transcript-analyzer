import { Component, Input } from '@angular/core';
import Script from 'src/app/core/models/script.model';
import Transcript from 'src/app/core/models/transcript.model';
import { AnalyzerCardConstants } from './analyzer-card.constants';

@Component({
  selector: 'app-analyzer-card',
  templateUrl: './analyzer-card.component.html',
  styleUrls: ['./analyzer-card.component.scss'],
})
export default class AnalyzerCardComponent {

  readonly CONSTANTS = AnalyzerCardConstants;

  @Input() selectedSensitivity = -1;
  @Input() cardType: 'realEnum' | 'expectedEnum' = 'realEnum';
  @Input() cardPercent = -1;
  @Input() call: Transcript | null = null;
  @Input() cardSide: 'transcript' | 'script' = 'transcript';
  @Input() cardOpposite: 'script' | 'transcript' = 'script';

  public handleMouseEnter = (from: 'transcript' | 'script', to: 'transcript' | 'script', order: number, highlighted: boolean) => {
    if (this.call && highlighted) {
      this.call[from][order].matching
        .map(script => script.matchingLine)
        .forEach(scriptIdx => {
          if (this.call) {
            this.call[to][scriptIdx].hovered = true;
          }
        });
    }
  }

  public handleMouseLeave = (to: 'transcript' | 'script') => {
    if (this.call) {
      (this.call[to] as Script[])
        .forEach(script => script.hovered = false);
    }
  }

  public tooltipDisabled = (script: Script): boolean => {
    if (this.cardType === 'realEnum') {
      return script.similarity != null && script.similarity < this.selectedSensitivity;
    }
    return false;
  }

  public highlighted = (script: Script): boolean => {
    if (this.cardType === 'realEnum') {
      return script.similarity != null && script.similarity >= this.selectedSensitivity && script.matching.length > 0;
    }
    return script != null && script.matching.length > 0;
  }
}
